package de.harambecoding.ahhboard;

import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.GridView;

public class MainActivity extends AppCompatActivity {

    public GridView gv1;
    public String[] soundnamen = {"Ahh 1", "Ahh 2", "Ahh 3", "Ahh 4", "Ahh 5", "Amena 1", "Amena 2", "Amena 3", "Amena 4", "Bahh",
    "Latino 1", "Latino 2", "Ah mein Bauch", "Schmerz", "Die Antwort", "Konzentrieren", "Na Klar",
    "Aldö ", "Hausaufgaben", "Hihi", "Scurr Scurr", "Neein", "OMG OMG OMG", "RöpsPorn", "7th Element" , "Ehm...", "Eskalation", "Hör auf zu Lachen", "YOLO", "Mähh"};
    public int[] soundId = {R.raw.ahh1, R.raw.ahh2, R.raw.ahh3, R.raw.ahh4, R.raw.ahh5, R.raw.amena1, R.raw.amena2, R.raw.amena3, R.raw.amena4, R.raw.bahh,
    R.raw.latino1, R.raw.latino2,  R.raw.ahmeinbauch, R.raw.schmerz, R.raw.antwort, R.raw.konzentrieren, R.raw.naklar,
    R.raw.aldoe, R.raw.hausaufgaben, R.raw.hihi, R.raw.scurrscurr, R.raw.nein, R.raw.omg, R.raw.ruelpsporn, R.raw.seventhelement, R.raw.daswarsdannauchschon1,
    R.raw.eskalieren, R.raw.hoeraufzulachen, R.raw.yolo,R.raw.maehh};
    public ArrayAdapter<String> listadapter;

    public MediaPlayer mp1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mp1 = MediaPlayer.create(this, R.raw.ahh1);

        gv1 = (GridView) findViewById(R.id.listView1);

        listadapter = new ArrayAdapter<String>(this, R.layout.list_item, soundnamen);
        gv1.setAdapter(listadapter);
        gv1.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view1, int i, long l) {

                mp1.release();
                mp1 = MediaPlayer.create(MainActivity.this, soundId[gv1.getPositionForView(view1)]);

                mp1.start();


            }
        });
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_info:
                // User chose the "Settings" item, show the app settings UI...
                startActivity(new Intent(MainActivity.this, Pop.class));
                return true;

            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);

        }
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.main, menu);
        return super.onCreateOptionsMenu(menu);
    }


    @Override
    protected void onPause() {
        mp1.stop();
        super.onPause();
    }


}
